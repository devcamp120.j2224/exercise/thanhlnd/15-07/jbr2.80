public class Rectangle extends Shape {

    double width = 1.0;
    double length = 1.0;

    public Rectangle(){

    }
    public Rectangle(double width , double length){
        this.width = width;
        this.length = length;
    }
    public Rectangle( double width , double length , String color, boolean filled) {
        super(color, filled);
        this.width = width ;
        this.length = length ;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    // diện tích dài * rộng
    public double getArea() {
        return width * this.length;
    }
    // chu vi dài + rộng nhân 2
    public double getPerimeter() {
        return (this.width + this.length) * 2 ;
    }

    @Override
    public String toString() {
        return "Rectangle [length=" + getWidth() + ", width=" + getLength() + " , phương thức lớp cha : " + super.toString() + "]";
    }
    
    
}
